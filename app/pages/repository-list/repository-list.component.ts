import { RepositoryIssue } from '../../models/repository-issue.model';
import { Repository } from '../../models/repository.model';
import { GithubService } from '../../services/github.service';
import tpl from './repository-list.component.html';
import './repository-list.component.scss';

class RepositoryListComponentController {
  repositories?: Repository[];
  selectedRepository?: Repository;

  keyword?: string;
  keywordChangeTimeout?: any;

  constructor(
    private $scope: any,
    private githubService: GithubService
  ) {
    $scope.$watch('$ctrl.keyword', () => {
      this.onKeywordChange();
    });
  }

  onKeywordChange() {
    if (this.keywordChangeTimeout) {
      clearTimeout(this.keywordChangeTimeout);
    }

    this.keywordChangeTimeout = setTimeout(() => {
      this.updateResults();
    }, 500);
  }

  onShowDetails(repository: Repository) {
    this.selectedRepository = repository;

    this.githubService.findIssuesByRepository(repository).then((issues: RepositoryIssue[]) => {
      if (this.selectedRepository) {
        this.selectedRepository.issues = issues;
      }
    });
  }

  private updateResults() {
    if (this.keyword) {
      this.githubService.searchRepositories(this.keyword).then(repositories => {
        this.repositories = repositories;
      });
    } else {
      this.repositories = [];
    }
  }
}

export const RepositoryListComponent = {
  template: tpl,
  controller: RepositoryListComponentController
}
