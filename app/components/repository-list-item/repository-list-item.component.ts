import { Repository } from '../../models/repository.model';
import tpl from './repository-list-item.component.html';
import './repository-list-item.component.scss';

export class RepositoryListItemComponentController {
  repository?: Repository;
}

export const RepositoryListItemComponent = {
  template: tpl,
  controller: RepositoryListItemComponentController,
  bindings: {
    repository: '='
  }
}
