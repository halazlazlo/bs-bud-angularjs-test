import { Repository } from '../../models/repository.model';
import tpl from './repository-details.component.html';
import './repository-details.component.scss';

class RepositoryDetailsComponentController {
  repository?: Repository;
}

export const RepositoryDetailsComponent = {
  template: tpl,
  controller: RepositoryDetailsComponentController,
  bindings: {
    repository: '='
  }
}
