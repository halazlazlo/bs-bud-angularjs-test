import tpl from './heading.component.html';
import './heading.component.scss';

class HeadingComponentController {
  type?: number;

  $onInit() {
    if (!this.type) {
      this.type = 2;
    }
  }
}

export const HeadingComponent = {
  template: tpl,
  controller: HeadingComponentController,
  transclude: true,
  bindings: {
    type: '<'
  }
}
