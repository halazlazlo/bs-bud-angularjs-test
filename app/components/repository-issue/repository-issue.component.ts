import { RepositoryIssue } from '../../models/repository-issue.model';
import tpl from './repository-issue.component.html';
import './repository-issue.component.scss';

class RepositoryIssueComponentController {
  issue?: RepositoryIssue;
}

export const RepositoryIssueComponent = {
  template: tpl,
  controller: RepositoryIssueComponentController,
  bindings: {
    issue: '='
  }
}
