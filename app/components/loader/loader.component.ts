import { LoaderService } from '../../services/loader.service';
import tpl from './loader.component.html';
import './loader.component.scss';

export class LoaderComponentController {
  isLoading: boolean = false;

  constructor(
    private $scope: any,
    private loaderService: LoaderService
  ) {
    $scope.$on('loaderService:isLoadingChange', (event: any, isLoading: boolean) => {
      this.isLoading = isLoading;
    });
  }
}

export const LoaderComponent = {
  template: tpl,
  controller: LoaderComponentController
}
