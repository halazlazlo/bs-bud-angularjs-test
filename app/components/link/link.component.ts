import tpl from './link.component.html';
import './link.component.scss';

export const LinkComponent = {
  template: tpl,
  bindings: {
      url: '<',
      target: '<'
  },
  transclude: true
}
