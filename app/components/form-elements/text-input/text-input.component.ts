import tpl from './text-input.component.html';
import './text-input.component.scss';

class TextInputComponentController {
  model?: string;
}

export const TextInputComponent = {
  template: tpl,
  controller: TextInputComponentController,
  bindings: {
    model: '=',
    label: '<'
  }
}
