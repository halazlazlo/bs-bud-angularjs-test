import { LoaderService } from "./loader.service";

export class HttpService {
  constructor(
    private $http: any,
    private loaderService: LoaderService
  ) {}

  get(url: string, params: any): Promise<any> {
    // start loader
    const loaderKey = `${url} + ${JSON.stringify(params)}`;
    this.loaderService.add(loaderKey);

    return this.$http.get(url, {
      params: params
    }).then((response: any) => {
      // finish loader
      this.loaderService.finish(loaderKey);

      return response.data;
    }).catch(() => {
      this.loaderService.finish(loaderKey);
    });
  }
}
