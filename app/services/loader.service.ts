export class LoaderService {
  private pile: string[] = [];
  // private subject = new BehaviorSubject<boolean>(false);

  constructor(private $rootScope: any) {}

  add(key: string) {
    this.pile.push(key);

    this.$rootScope.$broadcast('loaderService:isLoadingChange', true);
  }

  finish(key: string) {
    this.pile.splice(this.pile.indexOf(key), 1);

    if (!this.pile.length) {
      this.$rootScope.$broadcast('loaderService:isLoadingChange', false);
    }
  }
}
