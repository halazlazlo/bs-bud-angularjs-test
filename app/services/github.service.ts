import { RepositoryIssue } from '../models/repository-issue.model';
import { Repository } from '../models/repository.model';
import { HttpService } from './http.service';

export class GithubService {
  private baseUrl = 'https://api.github.com';

  constructor(
    private httpService: HttpService
  ) {}

  searchRepositories(keyword: string): Promise<Repository[]> {
    const url = `${this.baseUrl}/search/repositories`;

    return this.httpService.get(url, {
      q: keyword
    }).then((vndResponse: any) => {
      // return repos
      const repositories: Repository[] = [];

      vndResponse.items.forEach((vndEntity: any) => {
        repositories.push(Repository.createFromVndEntity(vndEntity));
      });

      return repositories;
    });
  }

  findIssuesByRepository(repository: Repository): Promise<RepositoryIssue[]> {
    const url = `${this.baseUrl}/search/issues`;

    return this.httpService.get(url, {
      q: `repo:${repository.name}`
    }).then((vndResponse: any) => {
      const issues: RepositoryIssue[] = [];

      vndResponse.items.forEach((vndEntity: any) => {
        issues.push(RepositoryIssue.createFromVndEntity(vndEntity));
      });

      return issues;
    });
  }
}
