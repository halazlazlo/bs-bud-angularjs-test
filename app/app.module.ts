import * as angular from 'angular';
import AppComponent from './app.component';
import AppRouting from './app.routing';
import { TextInputComponent } from './components/form-elements/text-input/text-input.component';
import { HeaderComponent } from './components/header/header.component';
import { HeadingComponent } from './components/heading/heading.component';
import { LinkComponent } from './components/link/link.component';
import { LoaderComponent } from './components/loader/loader.component';
import { RepositoryDetailsComponent } from './components/repository-details/repository-details.component';
import { RepositoryIssueComponent } from './components/repository-issue/repository-issue.component';
import { RepositoryListItemComponent } from './components/repository-list-item/repository-list-item.component';
import { RepositoryListComponent } from './pages/repository-list/repository-list.component';
import { GithubService } from './services/github.service';
import { HttpService } from './services/http.service';
import { LoaderService } from './services/loader.service';

export default angular.module('app', ['ui.router'])
    // pages
    .component('repositoryListComponent', RepositoryListComponent)

    // components
    .component('app', AppComponent)
    .component('appHeader', HeaderComponent)
    .component('appHeading', HeadingComponent)
    .component('appLink', LinkComponent)
    .component('appLoader', LoaderComponent)
    .component('appRepositoryDetails', RepositoryDetailsComponent)
    .component('appRepositoryIssue', RepositoryIssueComponent)
    .component('appRepositoryListItem', RepositoryListItemComponent)
    .component('appTextInput', TextInputComponent)

    // services
    .service('githubService', GithubService)
    .service('httpService', HttpService)
    .service('loaderService', LoaderService)

    // routing
    .config(AppRouting);
