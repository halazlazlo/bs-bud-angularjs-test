export class RepositoryIssue {
  content: string;
  userName: string;

  constructor(init: {
    content: string;
    userName: string;
  }) {
    this.content = init.content;
    this.userName = init.userName;
  }

  static createFromVndEntity(vndEntity: any) {
    return new RepositoryIssue({
      content: vndEntity.body,
      userName: vndEntity.user.login
    });
  }
}
