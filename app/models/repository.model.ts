import { RepositoryIssue } from "./repository-issue.model";

export class Repository {
  createdAt: Date;
  description: string;
  name: string;
  openIssuesCount: number;
  url: string;
  issues?: RepositoryIssue[]|null;

  constructor(init: {
    createdAt: Date;
    description: string;
    name: string;
    openIssuesCount: number;
    url: string;
    issues?: RepositoryIssue[];
  }) {
    this.createdAt = init.createdAt;
    this.description = init.description;
    this.name = init.name;
    this.openIssuesCount = init.openIssuesCount;
    this.url = init.url;
    this.issues = init.issues;
  }

  static createFromVndEntity(vndEntity: any): Repository {
    return new Repository({
      createdAt: vndEntity.created_at,
      description: vndEntity.description,
      name: vndEntity.full_name,
      openIssuesCount: vndEntity.open_issues,
      url: vndEntity.homepage,
    });
  }
}
