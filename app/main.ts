import * as angular from 'angular';
import appModule from './app.module';
import './assets/scss/main.scss';

angular.bootstrap(document, [appModule.name]);
