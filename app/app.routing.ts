export default ($stateProvider: any, $urlRouterProvider: any, $locationProvider: any) => {
  $stateProvider.state({
    name: 'Repository list',
    url: '/',
    component: 'repositoryListComponent'
  });

  $urlRouterProvider.otherwise('/');
  $locationProvider.html5Mode(true);
}
